import React, { useEffect, useState } from 'react'
import { Button, FlatList, ScrollView, StyleSheet, Text, TouchableHighlight, View } from 'react-native'
import AddPost from './components/PostAdd'
import Header from './components/Header'
import PostList from './components/PostList'

const App = () => {

  const [state, setState] = useState({ post: [] })
  const { post } = state


  const inputText = (newPost) => {
    const postArray = [...post]
    postArray.push(newPost)
    setState({ ...state, post: postArray })
  }

  return (
    <View style={styles.container}>
      <View>
        <Header />
      </View>
      <ScrollView>
        <AddPost inputParent={inputText} />
        <PostList post={post} />
      </ScrollView>
    </View>
  )
}

export default App

const styles = StyleSheet.create({
  container: { backgroundColor: '#eee', height: '100%' },
  text: { fontSize: 24, margin: '20%' }
})
