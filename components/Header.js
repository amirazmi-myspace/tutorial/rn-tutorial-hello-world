import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'

const Header = () => {

  const [state, setState] = useState({ user: 'Muhamad Amir' })

  const { user } = state

  console.log('jadi ke?')
  return (
    <View style={styles.container}>
      <Text style={styles.brand}>My Blog</Text>
      <Text style={styles.welcome}>Welcome {user}</Text>
    </View>
  )
}

export default Header

const styles = StyleSheet.create({
  container: { padding: '5%', flexDirection: 'row', alignItems: 'center' },
  brand: { fontSize: 20, color: '#333' },
  welcome: { fontSize: 20, marginLeft: 'auto', color: '#333' }
})
