import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const PostList = ({ post }) => {

  const _renderPost = () => {
    return post.map((item) => (
      <View style={styles.textbox}>
        <Text style={styles.text}>{item}</Text>
      </View>
    ))
  }

  return (
    <View style={styles.container}>
      {_renderPost()}
    </View>
  )
}

export default PostList

const styles = StyleSheet.create({
  container: { marginVertical: 20 },
  textbox: { paddingHorizontal: '5%', borderColor: '#ccc', borderTopWidth: 1, borderBottomWidth: 1, marginVertical: 10, backgroundColor: '#fff' },
  text: { fontSize: 18, paddingVertical: 20 }
})
