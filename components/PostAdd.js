import React, { useState } from 'react'
import { Button, Image, Modal, StyleSheet, Text, TextInput, TouchableHighlight, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const PostAdd = ({ inputParent }) => {

  const [state, setState] = useState({ modal: false, name: 'Muhamad Amir Azmi', post: '' })
  const { modal, name, post } = state
  const modalAdd = () => setState({ ...state, modal: !modal })

  const inputText = (e) => setState({ ...state, post: e })
  const submitPost = async () => {
    inputParent(post)
    setState({ ...state, post: '', modal: !modal })
  }

  const _renderAddBox = () => {
    return (
      <>
        <View style={styles.iconbox}>
          <Image source={require('../assets/profile.jpg')} style={styles.iconimage} />
        </View>
        <View style={styles.addbox}>
          <TouchableHighlight onPress={modalAdd} style={styles.touchbox} underlayColor='#ddd'><Text style={styles.addtext}>Add a new post</Text></TouchableHighlight>
        </View>
      </>
    )
  }

  const _renderModal = () => {
    return (
      <Modal animationType="slide" transparent={true} visible={modal}>
        <View style={styles.modalcontainer}>
          <View style={styles.modalheader}>
            <View style={styles.modalback}>
              <Icon name="arrow-back-outline" size={30} onPress={modalAdd}></Icon>
            </View>
            <View style={styles.modaltitle}>
              <Text style={styles.modaltitletext}>Create Post</Text>
            </View>
          </View>
          <View style={styles.modalbody}>
            <View style={styles.profile}>
              <View style={styles.profileimage}>
                <Image source={require('../assets/profile.jpg')} style={styles.profileimageimg} />
              </View>
              <View style={styles.profilename}>
                <Text style={styles.profilenametext}>{name}</Text>
              </View>
            </View>
            <View style={styles.input}>
              <TextInput placeholder="What's on your mine?" multiline style={styles.inputbox} onChangeText={(e) => inputText(e)} value={post} />
              <Button title='Post' disabled={post === '' && true} onPress={submitPost} />
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  return (
    <View style={styles.container}>
      {_renderAddBox()}
      {_renderModal()}

    </View>
  )
}

export default PostAdd

const styles = StyleSheet.create({
  container: { height: 50, paddingLeft: '5%', paddingRight: '5%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
  iconbox: { height: 50, flex: 2 },
  iconimage: { width: 50, height: 50, borderRadius: 50 / 2 },
  addbox: { flex: 10, height: '68%', alignSelf: 'center', borderRadius: 30, borderColor: '#333', borderWidth: 1 },
  touchbox: { borderRadius: 30, borderColor: '#ccc', borderWidth: 1, backgroundColor: '#fff' },
  addtext: { width: '100%', height: '100%', textAlign: 'center', textAlignVertical: 'center' },
  modalcontainer: { flex: 1, backgroundColor: '#fff' },
  modalheader: { height: 50, flexDirection: 'row', paddingHorizontal: '5%', alignItems: 'center', justifyContent: 'center', borderBottomWidth: 1, borderBottomColor: '#ccc' },
  modalbody: { flex: 93 },
  modalback: { flex: 10 },
  modaltitle: { flex: 90 },
  modaltitletext: { fontSize: 20 },
  profile: { height: 70, paddingHorizontal: '5%', paddingTop: 20, flexDirection: 'row', alignItems: 'flex-start' },
  input: { flex: 93, paddingHorizontal: '5%' },
  inputbox: { fontSize: 24 },
  profileimage: { flex: 13 },
  profileimageimg: { height: '100%', width: '100%', borderRadius: 100 / 2 },
  profilename: { flex: 87, marginLeft: 20 },
  profilenametext: { fontSize: 20 }
})
